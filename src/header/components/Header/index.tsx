import Image from "next/image";

import { NavItem } from "@/header/components/NavItem";
import { NAV_ITEMS } from "@/header/constants";

export function Header(): JSX.Element {
  return (
    <header className="mb-10 py-6">
      <nav className="flex max-h-[40px] max-w-fit justify-center">
        <Image
          className="mr-4"
          src="/clur-logo.svg"
          width={100}
          height={24}
          alt="Logo"
        />
        <ul className="hidden lg:flex">
          {NAV_ITEMS.map((item) => (
            <NavItem key={item.id} name={item.name} link={item.link} />
          ))}
        </ul>
      </nav>
    </header>
  );
}
