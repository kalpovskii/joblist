import Link from "next/link";

interface Props {
  name: string;
  link: string;
}
export function NavItem(props: Props): JSX.Element {
  const { name, link } = props;

  return (
    <>
      <Link href={link}>
        <li className="mr-3 cursor-pointer rounded-lg border border-slate-200 bg-white px-3 py-1.5">
          {name}
        </li>
      </Link>
    </>
  );
}
