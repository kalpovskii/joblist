export const NAV_ITEMS = [
  {
    id: 1,
    name: "Вакансии",
    link: "/",
  },
  {
    id: 2,
    name: "Компании",
    link: "/companies",
  },
  {
    id: 3,
    name: "Зарплаты",
    link: "/salaries",
  },
];
