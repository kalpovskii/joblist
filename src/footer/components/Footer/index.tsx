import Image from "next/image";
import Link from "next/link";

export function Footer(): JSX.Element {
  return (
    <footer>
      <div className="mb-2 flex justify-center">
        <span className="mr-2">Hiring Blockchain Talent?</span>
        <Link className="bg-gray-300 p-2" href="/post-job">
          Post a job
        </Link>
      </div>
      <div className="flex justify-center">
        <Image
          src="/footer-logo.svg"
          alt="footer logo"
          width={52}
          height={52}
        />
        <Image
          className="ml-2"
          src="/footer-logo.svg"
          alt="footer logo"
          width={52}
          height={52}
        />
        <Image
          className="ml-2"
          src="/footer-logo.svg"
          alt="footer logo"
          width={52}
          height={52}
        />
      </div>
    </footer>
  );
}
