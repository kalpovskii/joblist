import { NextApiRequest, NextApiResponse } from "next";
import { NextResponse } from "next/server";
import { sql } from "@vercel/postgres";

// export async function GET(request: Request) {
//   const { searchParams } = new URL(request.url);
//   const petName = searchParams.get("petName");
//   const ownerName = searchParams.get("ownerName");
//
//   try {
//     if (!petName || !ownerName) throw new Error("Pet and owner names required");
//     await sql`INSERT INTO Pets (Name, Owner) VALUES (${petName}, ${ownerName});`;
//   } catch (error) {
//     return NextResponse.json({ error }, { status: 500 });
//   }
//
//   const pets = await sql`SELECT * FROM Pets;`;
//   return NextResponse.json({ pets }, { status: 200 });
// }

// export default function handler(req: NextApiRequest, res: NextApiResponse) {
//   if (req.method === "POST") {
//     try {
//       return req.body;
//     } catch (error) {
//       return NextResponse.json({ error }, { status: 500 });
//     }
//   } else {
//     return "not post";
//   }
// }

interface VacancyData {
  role: string;
  company: string;
  date: number;
  city: string;
  salary: number;
  description: string;
  about: string;
}

export async function POST(req: Request) {
  const data: VacancyData = await req.json();
  const { role, company, date, city, salary, description, about } = data;

  try {
    if (!data) throw new Error("data is required");
    await sql`INSERT INTO Vacancies (Role, Company, Date, City, Salary, Description, About) VALUES (${role}, ${company}, ${date}, ${city}, ${salary}, ${description}, ${about});`;
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }

  const vacancies = await sql`SELECT * FROM Vacancies;`;
  return NextResponse.json({ vacancies }, { status: 200 });
}
