import { NextResponse } from "next/server";
// import { vacancies } from "./vacancies";
import { sql } from "@vercel/postgres";

// const postsPerPage = 5;
export async function GET(req: Request) {
  // const { searchParams } = new URL(req.url);
  //
  // const query = searchParams.get("query");
  // const pageParam = searchParams.get("page");
  // let page = pageParam ? parseInt(pageParam, 10) : 1;
  //
  // if (Number.isNaN(page) || page < 1) {
  //   page = 1;
  // }
  //
  // const startIndex = 0;
  // const endIndex = page * postsPerPage;
  //
  // let currentPosts = vacancies;
  // if (query) {
  //   currentPosts = vacancies.filter((post) =>
  //     post.tags.toLowerCase().includes(query.toLowerCase())
  //   );
  // }
  //
  // const limitedPosts = currentPosts.slice(startIndex, endIndex);
  //
  // return NextResponse.json(limitedPosts);
  const vacancies = await sql`SELECT * FROM Vacancies;`;

  return NextResponse.json({ vacancies }, { status: 200 });
}
