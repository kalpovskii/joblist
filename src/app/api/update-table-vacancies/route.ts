import { NextResponse } from "next/server";
import { sql } from "@vercel/postgres";

export async function GET(request: Request) {
  try {
    const result = await sql`CREATE TABLE Vacancies
      ADD COLUMN Date DATE,
      ADD COLUMN City varchar(100),
      ADD COLUMN Salary DECIMAL(10, 2),
      ADD COLUMN Description TEXT,
      ADD COLUMN About TEXT;`;
    return NextResponse.json({ result }, { status: 200 });
  } catch (error) {
    return NextResponse.json({ error }, { status: 500 });
  }
}
