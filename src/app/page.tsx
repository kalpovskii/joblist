import { Ads } from "@/vacancies/components/Ads/Ads";
import { NewJobsItem } from "@/vacancies/components/Jobs/NewJobsItem";
import { TitleSearch } from "@/vacancies/components/Vacancy/TItleSearch";

export default function Home(): JSX.Element {
  return (
    <main>
      <TitleSearch />
      <Ads />
      <NewJobsItem />
    </main>
  );
}
