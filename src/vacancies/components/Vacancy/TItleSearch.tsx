export function TitleSearch(): JSX.Element {
  return (
    <div className="mb-8 text-center">
      <span className="mb-4 block text-4xl font-bold">Поиск работы в Web3</span>
      <span className="mx-auto block max-w-[555px]">
        Best Web3 jobs in Crypto & Web3 companies.Below you can also view talent
        profiles, benchmark Web3 salaries, read related blog posts and learn
        more in the FAQ.
      </span>
    </div>
  );
}
