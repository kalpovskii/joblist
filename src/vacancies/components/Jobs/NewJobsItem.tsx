"use client";

import Image from "next/image";

import { useVacancies } from "@/vacancies/hooks/vacancies";

export function NewJobsItem(): JSX.Element {
  const { vacancies, isLoading } = useVacancies();

  if (isLoading) {
    return <div>loading...</div>;
  }

  return (
    <>
      {vacancies &&
        vacancies.map((vacancy: any) => (
          <div
            key={vacancy.id}
            className="mb-5 rounded-lg border border-black p-5"
          >
            <div className="mb-10 flex">
              <Image
                className="mr-2"
                src="/footer-logo.svg"
                alt="logo"
                width={52}
                height={52}
              />
              <div className="w-full">
                <div className="flex items-center justify-between">
                  <span className="block text-xl font-medium">
                    {vacancy.role}
                  </span>
                  <span className="block">{vacancy.date}</span>
                </div>
                <span className="block">{vacancy.company}</span>
              </div>
            </div>
            <div className="mb-10 flex items-center justify-between text-xl">
              <span>{vacancy.city}</span>
              <span>{vacancy.salary}</span>
            </div>
            <div className="mb-5">
              <span className="mb-2 block text-xl">стек:</span>
              <div className="flex">
                <span className="mr-2 block max-w-fit bg-gray-300 p-2">
                  masynya synya
                </span>
              </div>
            </div>
            <div className="mb-4">
              <span className="font-medium">Что делать:</span>
              <span className="block max-w-[320px]">{vacancy.description}</span>
            </div>
            <div className="flex items-end justify-between">
              <div>
                <span className="font-medium">О компании:</span>
                <span className="block max-w-[536px]">{vacancy.about}</span>
              </div>
              <a
                className="bg-gray-300 p-2 "
                href="https://github.com"
                target="_blank"
              >
                быстрый отклик
              </a>
            </div>
          </div>
        ))}
    </>
  );
}
