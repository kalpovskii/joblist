// import { useSearchParams } from "next/navigation";
import useSWR from "swr";

interface Vacancy {
  role: string;
  company: string;
  date: number;
  city: string;
  salary: number;
  description: string;
  about: string;
}
export function useVacancies() {
  const url = "/api/vacancies";

  const { data, error, isLoading } = useSWR(url, fetcher);
  const vacancies: Vacancy[] = data ? data.vacancies.rows : [];
  return {
    vacancies,
    error,
    isLoading,
  };
}
const fetcher = (url: string) => fetch(url).then((res) => res.json());
