const basicOptions = {
  ecmaVersion: "latest",
  sourceType: "module",
  ecmaFeatures: {
    jsx: true,
  },
};

const basicExtendsHead = ["airbnb", "plugin:prettier/recommended"];

const basicExtendsTail = ["next", "prettier"];


const basicRules = {
  "simple-import-sort/imports": "error",
  "import/extensions": [
    "error",
    {
      js: "never",
      jsx: "never",
      ts: "never",
      tsx: "never",
      json: true,
    },
  ],
  "import/no-extraneous-dependencies": ["warn"],
  "import/prefer-default-export": ["off"],
  "jsx-a11y/anchor-is-valid": ["off"],
  "jsx-a11y/label-has-associated-control": [
    "error",
    {
      depth: 3,
    },
  ],
  "no-console": [
    "error",
    {
      allow: ["info", "warn", "error"],
    },
  ],
  "no-unused-vars": [
    "error",
    {
      ignoreRestSiblings: true,
    },
  ],
  "no-use-before-define": [
    "error",
    {
      functions: false,
    },
  ],
  "no-param-reassign": [
    "error",
    {
      props: false,
    },
  ],
  "no-unsafe-optional-chaining": [
    "error",
    {
      disallowArithmeticOperators: false,
    },
  ],
  "react/jsx-no-constructed-context-values": ["off"],
  "react/no-unstable-nested-components": ["off"],
  "no-loss-of-precision": ["off"],
  "prefer-destructuring": ["off"],
  "react/function-component-definition": ["off"],
  "react/destructuring-assignment": ["off"],
  "react/display-name": ["off"],
  "react/jsx-filename-extension": ["off"],
  "react/jsx-props-no-spreading": ["off"],
  "react/jsx-uses-react": ["off"],
  "react/jsx-no-useless-fragment": ["off"],
  "react/prop-types": ["off"],
  "react/react-in-jsx-scope": ["off"],
  "react/require-default-props": ["off"],
  "@next/next/no-img-element": ["off"],
  "no-restricted-syntax": [
    "error",
    {
      selector:
          "BinaryExpression[operator='==='][left.type='UnaryExpression'][left.operator='typeof'][right.type='Literal'][right.value='number']",
      message:
          "Prefer Number.isFinite() to determine whether the passed value is a number",
    },
  ],
};

const basicSettings = {
  "import/parser": "@typescript-eslint/parser",
  "import/resolver": {
    webpack: {
      config: "webpack.config.js",
    },
  },
  react: {
    version: "detect",
  },
};

const basicGlobals = {
  process: "readonly",
  window: "readonly",
  document: false,
  navigator: false,
};

module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
  },
  parser: "@typescript-eslint/parser",
  overrides: [
    {
      files: ["*.js", "*.jsx"],
      parserOptions: basicOptions,
      extends: [...basicExtendsHead, ...basicExtendsTail],
      settings: basicSettings,
      globals: basicGlobals,
      rules: basicRules,
    },
    {
      files: ["*.ts", "*.tsx"],
      parserOptions: {
        ...basicOptions,
        tsconfigRootDir: __dirname,
        project: "./tsconfig.json",
      },
      plugins: ["@typescript-eslint", "simple-import-sort"],
      extends: [
        ...basicExtendsHead,
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:import/typescript",
        ...basicExtendsTail,
      ],
      settings: basicSettings,
      globals: basicGlobals,
      rules: {
        ...basicRules,
        "@typescript-eslint/no-misused-promises": ["off"],
        "no-shadow": ["off"],
        "no-unused-vars": ["off"],
        "no-use-before-define": ["off"],
        "no-void": ["off"],
        "@typescript-eslint/no-shadow": ["error"],
        "@typescript-eslint/no-unsafe-argument": ["off"],
        "@typescript-eslint/no-unsafe-call": ["off"],
        "@typescript-eslint/no-unsafe-return": ["off"],
        "@typescript-eslint/no-unsafe-assignment": ["off"],
        "@typescript-eslint/no-unsafe-member-access": ["off"],
        "@typescript-eslint/restrict-template-expressions": ["off"],
      },
    },
    {
      files: ["*.stories.js", "*.stories.ts", "*.stories.jsx", "*.stories.tsx"],
      rules: {
        "import/no-anonymous-default-export": "off",
      },
    },
    {
      files: ["*.d.ts"],
      rules: {
        "import/no-cycle": ["off"],
      },
    },
    {
      files: ["*.js", "*.jsx", "*.ts", "*.tsx"],
      rules: {
        "simple-import-sort/imports": [
          "error",
          {
            groups: [
              [
                "^react",
                "^next",
                "^@?\\w",
                "app.*api",
                "app.*types",
                "app.*constants",
                "app.*utils",
                "app.*mappers",
                "app.*helpers",
                "app.*layouts",
                "app.*store",
                "app.*hooks",
                "app.*components",
                "^\\.\\.(?!/?$)",
                "^\\.\\./?$",
                "^\\./(?=.*/)(?!/?$)",
                "^\\.(?!/?$)",
                "^\\./?$",
                "^.+\\.?(css)$",
              ],
            ],
          },
        ],
      },
    },
  ],
};
